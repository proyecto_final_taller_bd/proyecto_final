USE master
GO 
CREATE DATABASE BD_ELECCIONES 
ON
( NAME = elecciones_datos_principal,
FILENAME = 'D:\BD_SQLSERVER\PROYECTO_BASE_DE_DATOS\BASE_DE_DATOS_ELECCIONES\elecciones.mdf',
SIZE = 10,
MAXSIZE = 50,
FILEGROWTH = 10 )
LOG ON 
( NAME = horarios_log,
FILENAME = 'D:\BD_SQLSERVER\PROYECTO_BASE_DE_DATOS\BASE_DE_DATOS_ELECCIONES\elecciones.ldf',
SIZE = 5MB,
MAXSIZE = 25MB,
FILEGROWTH = 5MB );
GO

USE BD_ELECCIONES 
CREATE TABLE PERSONA (  
	ID INT PRIMARY KEY IDENTITY,
	CI VARCHAR (11) UNIQUE,
	NOMBRE VARCHAR (40) NOT NULL,
	AP_P VARCHAR (25) NOT NULL,
	AP_M VARCHAR (25) NOT NULL,
	FECHA_DE_NACIMIENTO DATE NOT NULL,
	SEXO CHAR(1) NOT NULL CHECK(SEXO IN('M','F')),
	DIRECCION VARCHAR (50) NOT NULL,
	TELEFONO VARCHAR (15),
	CORREO VARCHAR (35),
	PADRE VARCHAR (75) NOT NULL,
	MADRE VARCHAR (75) NOT NULL,
	SERIE_CI INT NOT NULL,
	SECCION_CI INT NOT NULL,
	ESTADO BIT  NOT NULL,
	EXPEDIDO DATE NOT NULL,
	VIGENTE DATE NOT NULL,
)

CREATE TABLE PERSONAL 
(
	ID INT PRIMARY KEY IDENTITY,
	ID_PER INT NOT NULL,
	ESTADO BIT NOT NULL,
	FOREIGN KEY(ID_PER) REFERENCES PERSONA (ID)

)
CREATE TABLE EQUIPO 
(
	No_EQUIPO INT PRIMARY KEY IDENTITY,
	MARCA VARCHAR (10) NOT NULL,
	No_SERIE  VARCHAR (10)NOT NULL,
	DESCRIPCION VARCHAR (50)NOT NULL
)
CREATE TABLE OFICIOS 
(
	ID INT PRIMARY KEY IDENTITY,
	NOMBRE VARCHAR (40) NOT NULL,
	SUELDO INT NOT NULL,
	DESCRIPCION VARCHAR (60)
)

CREATE TABLE HORARIO 
(
	ID INT PRIMARY KEY IDENTITY,
	HORA_ENTRADA DATETIME NOT NULL,
	HORA_SALIDA DATETIME
)

CREATE TABLE AMBIENTE 
(
	ID INT PRIMARY KEY IDENTITY,
	ESTADO BIT,
	NOMBRE  VARCHAR (40) NOT NULL,
	SUPERFICIE CHAR (10),
	PISO CHAR (10)
)


CREATE TABLE TIPO_DE_ELECCION(
	ID INT PRIMARY KEY IDENTITY,
	NOMBRE VARCHAR (50) NOT NULL,
	DESCRIPCION VARCHAR (100) NOT NULL
)

CREATE TABLE GESTION 
(
	ID INT PRIMARY KEY IDENTITY,
	NOMBRE DATE NOT NULL,
	ID_TIP_ELEC INT NOT NULL,
	FOREIGN KEY (ID_TIP_ELEC) REFERENCES TIPO_DE_ELECCION (ID)
)

CREATE TABLE ROL(
	ID INT PRIMARY KEY IDENTITY,
	NOMBRE_ROL VARCHAR (30) NOT NULL,
	DESCRIPCION VARCHAR(50) NOT NULL,

)


CREATE TABLE DESIGNAN  
(
	ID INT PRIMARY KEY IDENTITY,
	ID_PERSONAL INT FOREIGN KEY REFERENCES PERSONAL (ID),
	No_EQUIPO INT FOREIGN KEY REFERENCES EQUIPO (NO_EQUIPO),
	FECHA DATE NOT NULL
)
CREATE TABLE OFICINAS 
(
	ID INT PRIMARY KEY IDENTITY,
	ID_AMBIENTE INT FOREIGN KEY REFERENCES AMBIENTE (ID),
	NOMBRE VARCHAR (10) NOT NULL,
	SUPERFICIE CHAR (10) NOT NULL,
	UBICACION VARCHAR (20) NOT NULL,
	ID_HORARIO INT FOREIGN KEY REFERENCES HORARIO (ID),
	ESTADO BIT 
)


CREATE TABLE CONTRATO 
(
	ID INT PRIMARY KEY IDENTITY,
	ID_PERSONAL INT FOREIGN KEY REFERENCES PERSONAL(ID),
	ID_OFICIOS INT FOREIGN KEY REFERENCES OFICIOS (ID),
	ID_HORARIO INT FOREIGN KEY REFERENCES HORARIO (ID),
	FECHA_INICIO DATE NOT NULL,
	FECHA_EXPIRACION DATE NOT NULL,
	FECHA DATE
)
CREATE TABLE MESA 
(
	ID INT PRIMARY KEY IDENTITY,
	NOMBRE VARCHAR (15)
)

CREATE TABLE EMPADRONADO 
(
	ID INT PRIMARY KEY IDENTITY,
	CI_PER INT FOREIGN KEY REFERENCES PERSONA (ID),
	ID_PERSONAL INT FOREIGN KEY REFERENCES PERSONAL (ID),
	ID_MESA INT FOREIGN KEY REFERENCES MESA(ID),
	GRADO_INST VARCHAR (40),
	FIRMA TEXT,
	HUELLA_DAC TEXT NOT NULL,
	FOTOGRAFIA TEXT NOT NULL,
	NACIONALIDAD VARCHAR (25) NOT NULL
)
CREATE TABLE INHABILITADOS 
(
	ID_INA INT PRIMARY KEY,
	FOREIGN KEY (ID_INA) REFERENCES EMPADRONADO (ID),
	DESCRIPCION VARCHAR(50) NOT NULL,
	FECHA DATE NOT NULL
)
CREATE TABLE HABILITADOS(
	ID_HAB INT PRIMARY KEY,
	FECHA DATETIME NOT NULL,
	FOREIGN KEY (ID_HAB) REFERENCES EMPADRONADO (ID),
)

CREATE TABLE JURADO 
(
	ID INT PRIMARY KEY IDENTITY,
	ID_HABILITADOS INT FOREIGN KEY REFERENCES HABILITADOS (ID_HAB),
	ROL VARCHAR (25),
	DESCRIPCION VARCHAR (50)
)

CREATE TABLE ASIGNACION_MESA(
	ID INT PRIMARY KEY IDENTITY,
	ID_JUR INT NOT NULL,
	ID_MESA INT NOT NULL,
	FECHA DATE NOT NULL,
	FOREIGN KEY (ID_JUR) REFERENCES JURADO (ID),
	FOREIGN KEY (ID_MESA) REFERENCES MESA (ID),
)


CREATE TABLE PARTIDO 
(
	ID INT PRIMARY KEY IDENTITY,
	ID_TIP_ELEC INT FOREIGN KEY REFERENCES TIPO_DE_ELECCION (ID),
	NOMBRE VARCHAR (60),
	ESTRUCTURA_ORGANIZATIVA VARCHAR (30),
	SEDE VARCHAR (50),
	IDEOLOGIA_POLITICA VARCHAR (25),
	SIGLA CHAR (20),
	ESTATUTO VARCHAR (50)
)

CREATE TABLE CARTERA 
(
	ID INT PRIMARY KEY IDENTITY,
	ID_TIP_ELEC INT FOREIGN KEY REFERENCES TIPO_DE_ELECCION (ID),
	CARGO VARCHAR (25),
	DESCRIPCION VARCHAR (100)
)

CREATE TABLE PERTENECE_PARTIDO(
	ID INT PRIMARY KEY IDENTITY,
	ID_PART INT NOT NULL,
	ID_PER INT NOT NULL,
	FOREIGN KEY (ID_PART) REFERENCES PARTIDO (ID),
	FOREIGN KEY (ID_PER) REFERENCES PERSONA (ID),
)

CREATE TABLE POSTULANTE 
(
	ID INT PRIMARY KEY IDENTITY,
	ID_PER_PART INT NOT NULL,
	REQUISITO BIT,
	FOREIGN KEY(ID_PER_PART) REFERENCES PERTENECE_PARTIDO(ID),
)

CREATE TABLE VOTAR 
(
	ID INT PRIMARY KEY IDENTITY,
	ID_PART INT FOREIGN KEY REFERENCES PARTIDO(ID),
	ID_JUR INT FOREIGN KEY REFERENCES JURADO (ID),
	ID_HAB INT FOREIGN KEY REFERENCES HABILITADOS (ID_HAB),
)

CREATE TABLE DISTRITO 
(
	ID INT PRIMARY KEY IDENTITY,
	NOMBRE VARCHAR (25),
	DESCRIPCION VARCHAR(50)
)
CREATE TABLE MUNICIPIO(
	ID INT PRIMARY KEY IDENTITY,
	NOMBRE VARCHAR (25) NOT NULL,
	POBLACION INT NOT NULL,
)

CREATE TABLE DEPARTAMENTO(
	ID INT PRIMARY KEY IDENTITY,
	NOMBRE VARCHAR (25) NOT NULL,
	POBLACION INT NOT NULL,
)

CREATE TABLE PAIS 
(
	ID INT PRIMARY KEY IDENTITY,
	NOMBRE VARCHAR (25),
	REGION VARCHAR (25),
	POBLACION INT 
)

CREATE TABLE RECINTO_ELECTORAL(
	ID INT PRIMARY KEY IDENTITY,
	ID_ASI_MES INT NOT NULL,
	ID_DIST INT NOT NULL,
	ID_MUN INT NOT NULL,
	ID_DEP INT NOT NULL,
	ID_PAIS INT NOT NULL,
	DIRECCION VARCHAR (30) NOT NULL,
	DESCRIPCION VARCHAR(60) NOT NULL,
	NOMBRE VARCHAR(50) NOT NULL,
	FOREIGN KEY (ID_ASI_MES) REFERENCES ASIGNACION_MESA (ID),
	FOREIGN KEY (ID_DIST) REFERENCES DISTRITO (ID),
	FOREIGN KEY (ID_MUN) REFERENCES MUNICIPIO (ID),
	FOREIGN KEY (ID_DEP) REFERENCES DEPARTAMENTO (ID),
	FOREIGN KEY (ID_PAIS) REFERENCES PAIS (ID),
)




CREATE TABLE DIRECCION 
(
	ID INT PRIMARY KEY IDENTITY,
	ID_PER INT FOREIGN KEY REFERENCES PERSONA (ID),
	LUGAR VARCHAR (25),
	DIRECCION VARCHAR (25),
	DESCRIPCION VARCHAR (50)
)


CREATE TABLE PARTICIPANTE(
	ID INT PRIMARY KEY IDENTITY,
	ID_TIP_CART INT NOT NULL,
	ID_POST INT NOT NULL,
	FECHA DATE NOT NULL,
	FOREIGN KEY (ID_TIP_CART) REFERENCES CARTERA (ID),
	FOREIGN KEY (ID_POST) REFERENCES POSTULANTE (ID),
)




CREATE TABLE OCUPACION(
	ID INT PRIMARY KEY IDENTITY,
	ID_PER INT NOT NULL,
	NOMBRE VARCHAR(25) NOT NULL,
	DESCRIPCION VARCHAR(40) NOT NULL,
	FOREIGN KEY (ID_PER) REFERENCES PERSONA (ID),
)





CREATE TABLE USUARIO (
	ID INT PRIMARY KEY IDENTITY,
	ID_PER INT NOT NULL,
	NOMBRE_U VARCHAR(30)NOT NULL,
	CONTRASEA  VARCHAR(30)NOT NULL,
	FOREIGN KEY (ID_PER) REFERENCES PERSONAL (ID),
)


CREATE TABLE USUARIO_ROL(
	ID INT PRIMARY KEY IDENTITY,
	ID_ROL INT NOT NULL,
	ID_USU INT NOT NULL,
	FOREIGN KEY (ID_ROL) REFERENCES ROL (ID),
	FOREIGN KEY (ID_USU) REFERENCES USUARIO	 (ID),

)

CREATE TABLE ESCRUTINIO (
	ID INT PRIMARY KEY IDENTITY,
	ID_PART INT NOT NULL,
	ID_ASI_MES INT NOT NULL,
	VOTOS_BLANCOS INT NOT NULL,
	VOTSO_NULOS INT NOT NULL,
	VOTOS_RECIBIDOS INT NOT NULL,
	DESCRIPCION VARCHAR (50) NOT NULL,
	FECHA DATE NOT NULL,
	FOREIGN KEY (ID_PART) REFERENCES PARTICIPANTE (ID),
	FOREIGN KEY (ID_ASI_MES) REFERENCES ASIGNACION_MESA (ID),
)
