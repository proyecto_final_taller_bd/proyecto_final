

INSERT INTO PERSONA(CI,NOMBRE,AP_P,AP_M,FECHA_DE_NACIMIENTO,SEXO,DIRECCION,TELEFONO,CORREO,PADRE,MADRE,SERIE_CI,SECCION_CI,ESTADO,EXPEDIDO,VIGENTE) VALUES
('7342412', 'Cacilia Northing', 'Joinson', 'Northing', '2023-03-16', 'F', '55 Beilfuss Pass', '115-438', 'dnorthing0@ovh.net', 'Joanna Northing', 'Dari Northing', 53979, 10571, 0, '2022-10-25', '2023-05-16'),
('8695304', 'Adda Perrett', 'Folland', 'Perrett', '2022-11-28', 'F', '6 Claremont Hill', '817-516', 'rperrett1@domainmarket.com', 'Traci Perrett', 'Ronica Perrett', 31771, 64483, 0, '2022-07-02', '2023-02-05'),
('6490209', 'Bondon Fidgin', 'Beatens', 'Fidgin', '2022-10-03', 'F', '738 Sunbrook Pass', '769-115', 'cfidgin2@google.co.uk', 'Trude Fidgin', 'Cathlene Fidgin', 98320, 71245, 1, '2022-07-22', '2022-08-16'),
('3834466', 'Edouard Strainge', 'Brislan', 'Strainge', '2022-06-12', 'M', '4416 Coolidge Park', '723-581', 'mstrainge3@dion.ne.jp', 'Renard Strainge', 'Mozelle Strainge', 83897, 18894, 1, '2022-10-12', '2022-06-23');


insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (88045128, 'Ashbey', 'Hardern', 'Goldine', '1990-07-28', 'M', 'Gerald', 93720826, 'kgoldine0@goo.gl', 'Ingelbert', 'Kean', 47773, 64328, 1, '2022-05-25', '2024-06-18');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (14483458, 'Kellby', 'McCord', 'Gerbi', '1998-11-14', 'M', 'Kim', 76229836, 'rgerbi1@ca.gov', 'Ripley', 'Rickard', 48597, 61980, 0, '2021-10-07', '2023-12-25');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (61901509, 'Aleda', 'Pounder', 'Kollas', '1999-04-05', 'F', 'Boyd', 57823947, 'gkollas2@fotki.com', 'Cherlyn', 'Gavrielle', 46180, 60107, 1, '2022-03-06', '2024-03-29');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (41954409, 'Ingram', 'Muress', 'Glavin', '1994-10-23', 'M', 'Clarendon', 88502388, 'tglavin3@liveinternet.ru', 'Wilden', 'Tripp', 46211, 51872, 0, '2022-10-11', '2024-11-19');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (52503480, 'Lizbeth', 'Camilli', 'Shovel', '1993-08-14', 'F', 'Nova', 95540921, 'yshovel4@npr.org', 'Morganne', 'Yetty', 49537, 57922, 0, '2022-05-20', '2024-10-17');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (75756806, 'Nealson', 'Alfonsetto', 'Sackes', '2001-02-02', 'M', 'Kipling', 36913557, 'tsackes5@icio.us', 'Virge', 'Tulley', 48331, 61187, 0, '2022-11-15', '2024-11-03');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (23137243, 'Myrah', 'Malpass', 'Boulding', '1994-03-02', 'F', 'Wayridge', 69233513, 'jboulding6@scientificamerican.com', 'Orelee', 'Jilli', 52258, 59301, 0, '2022-09-13', '2024-05-02');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (26930739, 'Mathew', 'Harwood', 'Ales0', '1996-03-01', 'M', 'Ramsey', 71831838, 'sales7@delicious.com', 'Xymenes', 'Sutton', 52933, 48404, 0, '2022-01-18', '2024-06-14');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (57143104, 'Enrika', 'Sapsforde', 'Hearsum', '1999-06-15', 'F', 'Lien', 30621105, 'khearsum8@tmall.com', 'Roseann', 'Katy', 48995, 57164, 1, '2022-05-29', '2024-07-07');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (69740525, 'Giavani', 'Troubridge', 'Paige', '1997-05-21', 'M', 'Moulton', 46668701, 'epaige9@sciencedaily.com', 'Tobie', 'Elmer', 53712, 52651, 0, '2021-06-28', '2024-12-11');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (18183042, 'Alecia', 'de Pinna', 'Pinkie', '2000-10-26', 'F', 'Iowa', 63384108, 'cpinkiea@360.cn', 'Florette', 'Courtney', 45922, 62704, 0, '2022-05-08', '2024-03-09');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (20024355, 'Sergent', 'Grossman', 'Gladden', '1992-02-05', 'M', 'Fair Oaks', 74213361, 'bgladdenb@a8.net', 'Fredric', 'Benjamen', 49600, 60738, 0, '2021-12-27', '2023-09-15');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (62754969, 'Boyce', 'Edwin', 'Patty', '1991-01-24', 'M', 'Caliangt', 85699445, 'zpattyc@people.com.cn', 'Hagan', 'Zollie', 45406, 66226, 0, '2022-01-07', '2024-06-22');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (42766024, 'Althea', 'Chrestien', 'Creenan', '2004-06-03', 'F', 'Lake View', 91315286, 'vcreenand@abc.net.au', 'Gizela', 'Vernice', 52510, 58176, 0, '2022-01-01', '2024-10-10');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (38703005, 'Bendix', 'Yerborn', 'Stitt', '1991-11-27', 'M', 'Goodland', 79820456, 'cstitte@utexas.edu', 'Jessee', 'Clerkclaude', 53777, 52654, 0, '2021-11-16', '2024-05-01');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (41391501, 'Violette', 'Caustick', 'Trainer', '2003-12-22', 'F', 'Atwood', 58588605, 'rtrainerf@bigcartel.com', 'Bobbee', 'Renae', 54793, 64394, 1, '2021-07-27', '2023-08-23');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (71610824, 'Harmony', 'Juppe', 'Hiom', '2003-12-24', 'F', 'Merry', 78639724, 'shiomg@amazon.com', 'Heddi', 'Selene', 54643, 45753, 0, '2022-02-22', '2024-09-16');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (35298504, 'Lazare', 'Slocum', 'Pharro', '2003-07-18', 'M', 'Fair Oaks', 35200705, 'tpharroh@blogs.com', 'Brendin', 'Thaxter', 52278, 55393, 1, '2022-06-02', '2023-10-27');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (87124046, 'Kari', 'Radbond', 'Burness', '1998-10-15', 'F', 'Blue Bill Park', 88002036, 'kburnessi@cafepress.com', 'Christel', 'Katerine', 53902, 58897, 0, '2021-10-03', '2024-12-09');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (51538332, 'Jocko', 'Lowensohn', 'Izod', '1997-03-25', 'M', 'Village Green', 67424727, 'kizodj@miitbeian.gov.cn', 'Nickie', 'Kippy', 52930, 48964, 0, '2021-08-29', '2024-01-04');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (15176485, 'Westleigh', 'Derobert', 'Battye', '1991-05-17', 'M', 'Paget', 32915323, 'tbattyek@army.mil', 'Dore', 'Thibaud', 53392, 56249, 1, '2022-10-24', '2024-12-29');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (79848733, 'Merle', 'Lippi', 'Lillie', '2002-08-27', 'M', 'Towne', 51307441, 'clilliel@facebook.com', 'Bax', 'Claybourne', 52618, 59922, 0, '2022-06-25', '2024-05-15');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (55328406, 'Annmaria', 'Meriel', 'Castille', '1997-11-15', 'F', 'Muir', 43550151, 'acastillem@senate.gov', 'Lissa', 'Amalita', 54780, 51390, 1, '2022-03-16', '2024-02-29');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (17407466, 'Cynthie', 'Stellino', 'Torregiani', '1990-10-06', 'F', 'Badeau', 38661708, 'atorregianin@sciencedirect.com', 'Cybill', 'Annamaria', 52373, 45033, 0, '2021-11-30', '2024-04-28');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (85481796, 'Lyndsie', 'Rydings', 'Jira', '1992-01-30', 'F', 'Coleman', 61467352, 'kjirao@smugmug.com', 'Lynea', 'Kial', 44603, 44946, 0, '2022-10-01', '2024-06-11');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (64905188, 'Betta', 'Crews', 'Sandys', '1996-09-03', 'F', 'Sundown', 76991798, 'gsandysp@booking.com', 'Corrie', 'Gretal', 44990, 64010, 1, '2021-11-27', '2024-01-30');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (75467736, 'Delores', 'Houlridge', 'Brachell', '1999-02-15', 'F', 'Oakridge', 69310783, 'dbrachellq@indiatimes.com', 'Ki', 'Darell', 50299, 53293, 1, '2022-01-03', '2024-08-13');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (16073850, 'Alistair', 'Brandt', 'Esilmon', '1998-04-09', 'M', 'Carioca', 91619620, 'gesilmonr@redcross.org', 'Doug', 'Gal', 52397, 58026, 1, '2021-12-14', '2024-04-18');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (71715683, 'Bil', 'Gleasane', 'Glavias', '1996-07-31', 'M', 'Delaware', 23875825, 'yglaviass@bloomberg.com', 'Wilton', 'Yvor', 48299, 60596, 0, '2021-10-18', '2023-10-12');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (89257690, 'Rurik', 'Brownsea', 'Forton', '1990-11-28', 'M', 'Forest Run', 64702814, 'mfortont@mapquest.com', 'Leslie', 'Marcus', 55087, 45532, 1, '2022-03-09', '2024-02-21');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (66321164, 'Tam', 'Kneath', 'Jennings', '1990-09-14', 'M', 'Tomscot', 55900427, 'bjenningsu@samsung.com', 'Jaime', 'Bengt', 52059, 46103, 1, '2022-06-08', '2023-09-15');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (42332159, 'Erhart', 'Golsthorp', 'Bartels', '1997-07-01', 'M', 'Erie', 44167316, 'pbartelsv@house.gov', 'Franky', 'Porty', 45971, 46639, 0, '2022-02-20', '2023-10-04');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (36365570, 'Patrice', 'Hammell', 'Grizard', '1991-03-11', 'F', 'Oak Valley', 41805319, 'hgrizardw@abc.net.au', 'Millie', 'Hildy', 54719, 54097, 1, '2022-10-17', '2023-10-20');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (20654773, 'Reade', 'McNickle', 'Fisk', '2002-02-18', 'M', 'Thompson', 88846328, 'tfiskx@fastcompany.com', 'Stuart', 'Trumaine', 52413, 45895, 1, '2022-11-11', '2024-01-20');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (27071560, 'Christos', 'Demaid', 'Dudgeon', '2002-01-08', 'M', 'Crescent Oaks', 25797394, 'fdudgeony@tripadvisor.com', 'Orbadiah', 'Farleigh', 52137, 51248, 0, '2022-04-21', '2024-08-31');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (15808704, 'Josie', 'Christley', 'Craigs', '1990-07-27', 'F', 'Heath', 60174200, 'mcraigsz@telegraph.co.uk', 'Emmey', 'Mercie', 50891, 51514, 1, '2021-10-16', '2024-02-21');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (67090804, 'Sascha', 'Brownlie', 'Simco', '1999-09-30', 'M', 'Village', 90205645, 'tsimco10@intel.com', 'Tiebold', 'Tomlin', 46275, 63928, 1, '2021-10-05', '2024-09-10');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (88627313, 'Brittaney', 'Dracey', 'Dingwall', '1997-11-25', 'F', 'New Castle', 43916681, 'mdingwall11@dagondesign.com', 'Laurie', 'Martina', 46861, 64887, 0, '2021-11-24', '2024-04-28');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (57833473, 'Derrik', 'Anear', 'McGonnell', '1997-05-16', 'M', 'Ludington', 91487269, 'rmcgonnell12@bloglines.com', 'Dur', 'Riccardo', 55201, 57967, 0, '2021-12-03', '2024-05-16');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (53112823, 'Boot', 'Dunkirk', 'Heald', '2004-06-30', 'M', 'Barby', 27439895, 'rheald13@yolasite.com', 'Gerek', 'Raffaello', 48388, 64749, 0, '2022-01-02', '2023-09-22');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (36395775, 'Wat', 'Raffan', 'Farren', '2001-11-07', 'M', 'Loomis', 62832697, 'pfarren14@java.com', 'Heywood', 'Perren', 49455, 61212, 0, '2022-08-13', '2023-12-24');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (69237070, 'Virgil', 'Petric', 'Boase', '2003-10-10', 'M', 'Clarendon', 53238139, 'rboase15@unesco.org', 'Emilio', 'Ryon', 50498, 62871, 0, '2021-12-11', '2023-08-10');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (32812128, 'Franky', 'Cruddace', 'Oswell', '1990-09-28', 'M', 'Namekagon', 79912817, 'woswell16@shop-pro.jp', 'Matias', 'Wake', 44719, 50809, 1, '2021-12-08', '2024-01-12');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (23086794, 'Meredith', 'Davidofski', 'Povlsen', '1994-10-21', 'M', 'Orin', 67839187, 'apovlsen17@miibeian.gov.cn', 'Rodney', 'Arvin', 54088, 49023, 0, '2022-10-01', '2024-09-03');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (54253911, 'Dewain', 'Neathway', 'Yon', '1998-02-11', 'M', 'Spenser', 38105772, 'vyon18@phoca.cz', 'Dewitt', 'Valentijn', 45361, 65304, 1, '2021-07-13', '2024-10-16');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (90565322, 'Lurette', 'Innocent', 'Polye', '1996-06-30', 'F', 'Golf Course', 95815420, 'mpolye19@themeforest.net', 'Michaelina', 'Margi', 46355, 48794, 1, '2022-11-25', '2023-11-18');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (59212719, 'Wes', 'Noblett', 'Stoakley', '2004-01-20', 'M', 'Green', 99222191, 'istoakley1a@zdnet.com', 'Roderic', 'Iver', 49875, 45851, 0, '2022-06-07', '2024-12-05');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (54804084, 'Caitrin', 'Hinchcliffe', 'Vallintine', '2005-03-20', 'F', 'Nevada', 71320742, 'svallintine1b@pen.io', 'Amabel', 'Stacia', 53043, 47924, 0, '2022-01-25', '2024-03-10');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (19438239, 'Halie', 'Mulcahy', 'Picheford', '2002-12-01', 'F', 'Leroy', 82641608, 'gpicheford1c@ehow.com', 'Leelah', 'Gail', 52819, 59828, 0, '2021-08-20', '2024-12-13');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (35663822, 'Karyl', 'Miquelet', 'Bigley', '2003-11-29', 'F', 'Jenifer', 24522874, 'gbigley1d@csmonitor.com', 'Janeta', 'Georgianne', 52456, 66128, 1, '2022-01-19', '2023-10-21');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (34348947, 'Shelden', 'Kermeen', 'Deners', '2000-11-16', 'M', 'Drewry', 52834077, 'ddeners1e@dagondesign.com', 'Ode', 'Dewitt', 50160, 45015, 0, '2022-01-29', '2024-05-31');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (63647678, 'Branden', 'Mignot', 'Zanni', '2002-12-07', 'M', 'Michigan', 57103717, 'ezanni1f@yale.edu', 'Roderigo', 'Emilio', 49782, 52611, 0, '2022-03-20', '2024-01-08');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (62916327, 'Talbert', 'Mazzeo', 'Sones', '1998-08-02', 'M', 'Sauthoff', 29460836, 'gsones1g@etsy.com', 'Eddie', 'Geri', 54154, 54258, 1, '2021-06-21', '2024-02-16');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (66174201, 'Clare', 'Avramovitz', 'Maliffe', '2004-02-12', 'M', 'Killdeer', 25652104, 'lmaliffe1h@webnode.com', 'Clayborn', 'Ludwig', 47614, 51213, 1, '2021-12-29', '2024-06-03');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (46891365, 'Stanislas', 'Nelthropp', 'Friar', '1993-12-10', 'M', 'Clarendon', 99051770, 'sfriar1i@spotify.com', 'Archer', 'Scot', 50684, 65160, 1, '2022-05-17', '2023-10-30');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (23763449, 'Christy', 'Braban', 'Lampard', '1990-11-10', 'M', 'Green', 45519260, 'alampard1j@soup.io', 'Weider', 'Ara', 49759, 46907, 1, '2021-11-13', '2024-09-30');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (16792148, 'Todd', 'Ivins', 'McDell', '2004-01-14', 'M', 'High Crossing', 36784166, 'jmcdell1k@nydailynews.com', 'Andres', 'Jaye', 48985, 56009, 0, '2021-08-22', '2024-08-10');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (76632796, 'Rici', 'Parsonson', 'Pavluk', '2003-10-10', 'F', 'Bultman', 42260599, 'npavluk1l@chicagotribune.com', 'Lucila', 'Nissie', 50376, 58649, 0, '2021-08-26', '2024-08-22');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (19614789, 'Francoise', 'Heddan', 'Lambird', '2004-06-14', 'F', 'Glacier Hill', 53797081, 'klambird1m@webs.com', 'Natalie', 'Katlin', 46401, 60621, 0, '2022-10-24', '2024-01-05');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (18196667, 'Timothy', 'Stefanovic', 'Jiggen', '1997-03-07', 'M', 'Anzinger', 68731425, 'fjiggen1n@spiegel.de', 'Hubert', 'Finley', 46014, 63326, 1, '2021-06-26', '2024-10-18');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (14920697, 'Reynard', 'Lumpkin', 'Wieprecht', '2000-09-11', 'M', 'Vahlen', 29314032, 'rwieprecht1o@who.int', 'Kimbell', 'Reuben', 45845, 57636, 1, '2021-08-26', '2024-02-11');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (42700289, 'Janka', 'Piscopello', 'Newberry', '2002-12-08', 'F', 'Mayer', 21510244, 'anewberry1p@marriott.com', 'Maiga', 'Anya', 45452, 63792, 0, '2021-06-01', '2024-08-18');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (32651600, 'Skipton', 'Marjanovic', 'Daugherty', '1999-05-18', 'M', 'Bellgrove', 53337598, 'qdaugherty1q@uol.com.br', 'Ephrem', 'Quincey', 54656, 64242, 1, '2022-10-05', '2024-09-07');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (44390691, 'Eryn', 'Purdom', 'Tumilson', '2004-07-28', 'F', 'Dayton', 84927807, 'mtumilson1r@usnews.com', 'Allsun', 'Margaret', 51706, 45123, 1, '2021-09-28', '2024-11-24');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (51944292, 'De witt', 'Mee', 'Cardenoza', '2000-03-30', 'M', 'Parkside', 51367058, 'acardenoza1s@qq.com', 'Garvey', 'Anderson', 50356, 54117, 1, '2022-10-17', '2023-09-16');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (35100559, 'Waverly', 'Hildrew', 'Rawlingson', '2003-04-08', 'M', 'Haas', 37567708, 'irawlingson1t@cornell.edu', 'Wiatt', 'Isidore', 46791, 60357, 1, '2022-06-08', '2023-08-24');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (90007432, 'Petrina', 'Scaysbrook', 'Johnke', '1991-10-15', 'F', 'Cascade', 23229555, 'kjohnke1u@fotki.com', 'Calida', 'Katleen', 46071, 49866, 0, '2022-07-04', '2024-08-16');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (53008980, 'Pepillo', 'De Zuani', 'Verchambre', '2003-08-05', 'M', 'Mccormick', 37715787, 'cverchambre1v@usatoday.com', 'Noach', 'Christophorus', 52111, 60145, 1, '2022-10-06', '2024-01-23');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (52259692, 'Margalo', 'Paddle', 'Vargas', '2000-11-01', 'F', 'Crowley', 82732779, 'tvargas1w@dell.com', 'Gertrudis', 'Teodora', 45680, 50568, 0, '2022-02-04', '2023-11-29');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (53728671, 'Brenna', 'Treeby', 'Ismirnioglou', '2004-09-20', 'F', 'Sherman', 83773187, 'eismirnioglou1x@arstechnica.com', 'Raf', 'Elaina', 52601, 55469, 0, '2022-09-25', '2024-02-16');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (37790491, 'Codie', 'Liffey', 'Blown', '1993-03-20', 'M', 'Harbort', 84640979, 'eblown1y@free.fr', 'Colman', 'Ernest', 47936, 53552, 0, '2021-10-29', '2023-10-14');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (77361312, 'Onofredo', 'Fogg', 'Stickings', '2003-10-07', 'M', 'Independence', 83412435, 'fstickings1z@soundcloud.com', 'Giacomo', 'Floyd', 52449, 60567, 0, '2021-11-22', '2024-08-21');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (75585147, 'Benoit', 'Perago', 'Neeve', '1992-10-14', 'M', 'Moland', 44229418, 'kneeve20@eventbrite.com', 'Chaddie', 'Kermy', 51791, 47834, 0, '2021-10-26', '2023-09-18');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (15292974, 'Ivie', 'Busch', 'Singyard', '1997-05-30', 'F', 'Calypso', 65373623, 'vsingyard21@cnn.com', 'Gui', 'Viviene', 54411, 59208, 0, '2021-09-17', '2024-05-19');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (78065761, 'Marijn', 'Soda', 'Cuxon', '1998-09-14', 'M', 'Arapahoe', 87983744, 'ccuxon22@businesswire.com', 'Carlos', 'Conroy', 46039, 61549, 0, '2021-10-17', '2024-09-29');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (64705278, 'Bernadene', 'Jakubovicz', 'Piggford', '2001-02-26', 'F', 'Magdeline', 92542115, 'bpiggford23@huffingtonpost.com', 'Blanche', 'Bab', 55457, 63870, 0, '2021-07-12', '2024-08-10');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (68124307, 'Roberta', 'Rispine', 'Duiguid', '1997-07-27', 'F', 'Crescent Oaks', 86543422, 'sduiguid24@loc.gov', 'Leslie', 'Stormy', 45838, 54011, 0, '2021-11-10', '2024-08-19');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (60706834, 'Royall', 'Mouget', 'Brent', '1996-11-09', 'M', 'Knutson', 48887413, 'fbrent25@yellowpages.com', 'Zak', 'Franklyn', 51007, 56343, 0, '2022-06-11', '2024-01-31');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (15341544, 'Farrell', 'Kinningley', 'Sibthorp', '1991-08-15', 'M', 'Walton', 59972977, 'hsibthorp26@tinyurl.com', 'Sigismond', 'Harcourt', 51769, 58628, 1, '2022-01-12', '2023-08-11');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (41395853, 'Tome', 'Zuenelli', 'Mollon', '1999-03-26', 'M', 'Old Shore', 83191125, 'amollon27@blogtalkradio.com', 'Avery', 'Adolph', 47644, 55403, 0, '2021-12-03', '2024-12-06');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (39053626, 'Lay', 'Eilhersen', 'Paradis', '1995-02-19', 'M', 'Blue Bill Park', 21667467, 'aparadis28@wisc.edu', 'Dimitri', 'Angie', 49582, 65571, 1, '2021-07-16', '2024-06-13');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (29944694, 'Betti', 'Stiant', 'Haythornthwaite', '1994-08-04', 'F', 'Moose', 74885699, 'lhaythornthwaite29@icio.us', 'Lesli', 'Lizzie', 46799, 46695, 0, '2022-06-10', '2024-03-17');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (79829765, 'Dougy', 'Wyche', 'Cristoforo', '1992-05-04', 'M', 'Ludington', 94587065, 'rcristoforo2a@facebook.com', 'Ilaire', 'Rex', 51746, 56296, 1, '2022-09-01', '2023-09-30');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (43441985, 'Mead', 'Yoslowitz', 'Campos', '2000-11-30', 'M', 'Brickson Park', 25383325, 'bcampos2b@live.com', 'Rutger', 'Bradley', 55230, 53315, 0, '2022-01-20', '2025-01-01');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (60378986, 'Roselin', 'Gergolet', 'Jowle', '2005-01-23', 'F', 'Golf', 78896021, 'kjowle2c@ucoz.com', 'Hedda', 'Katalin', 48315, 57114, 1, '2022-06-06', '2024-01-01');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (42516779, 'Cody', 'Rewcassell', 'Blaszczyk', '2002-10-24', 'F', 'Lindbergh', 31441495, 'bblaszczyk2d@hubpages.com', 'Sibilla', 'Beatrisa', 50047, 61698, 0, '2022-05-16', '2023-10-07');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (58787323, 'Alley', 'Ungerer', 'Crehan', '1991-06-16', 'M', 'Jenifer', 74443082, 'rcrehan2e@lycos.com', 'Siegfried', 'Rayner', 55397, 50856, 1, '2022-01-12', '2024-12-05');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (22618260, 'Maurise', 'Mitroshinov', 'Kornilyev', '2000-05-14', 'M', 'Arizona', 98023444, 'akornilyev2f@marriott.com', 'Edsel', 'Abbey', 45247, 61810, 0, '2022-10-22', '2024-07-19');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (20989678, 'Steve', 'Dreger', 'Doiley', '2004-08-15', 'M', 'Main', 63938260, 'tdoiley2g@sohu.com', 'Brade', 'Terrill', 45982, 48458, 0, '2021-06-21', '2023-11-09');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (24585392, 'Ashlan', 'Perin', 'Youthed', '1998-04-16', 'F', 'Basil', 31545561, 'oyouthed2h@gmpg.org', 'Brunhilda', 'Orly', 52450, 65512, 0, '2021-08-16', '2024-06-23');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (84909491, 'Carey', 'Keelinge', 'Baskwell', '1995-10-19', 'F', 'Cottonwood', 46505135, 'kbaskwell2i@narod.ru', 'Rebeca', 'Kathye', 48586, 53955, 0, '2021-12-13', '2024-12-31');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (84541800, 'Lilias', 'Pashler', 'Brobak', '2001-09-12', 'F', 'Johnson', 99581732, 'jbrobak2j@storify.com', 'Shanie', 'Jaimie', 53528, 48330, 0, '2022-10-19', '2024-10-30');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (44232517, 'Amelina', 'McAuley', 'Lanigan', '2000-04-26', 'F', 'Graceland', 42399652, 'glanigan2k@chicagotribune.com', 'Adrea', 'Gracie', 46866, 53913, 0, '2021-11-24', '2024-02-13');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (55723459, 'Stormy', 'Laverenz', 'Caldayrou', '1991-08-03', 'F', 'Hauk', 69602916, 'ccaldayrou2l@rediff.com', 'Ursa', 'Claudelle', 46937, 65864, 0, '2022-05-18', '2024-05-31');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (66404983, 'Kermit', 'Klaes', 'Garces', '1992-11-03', 'M', 'Bartelt', 75953066, 'bgarces2m@liveinternet.ru', 'Odo', 'Boone', 53226, 49014, 1, '2022-09-17', '2023-11-23');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (43477773, 'Lauren', 'Grinval', 'Oakhill', '1995-09-25', 'M', 'Dexter', 86745319, 'woakhill2n@patch.com', 'Austen', 'Wakefield', 47565, 64237, 0, '2022-06-30', '2024-11-12');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (79729100, 'Taber', 'Cheers', 'MacDermid', '1996-12-31', 'M', 'Gateway', 48432362, 'amacdermid2o@spotify.com', 'Cedric', 'Austin', 54156, 52173, 1, '2022-10-28', '2023-09-02');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (23389886, 'Bernie', 'Whitear', 'Weatherill', '1993-10-17', 'F', 'Daystar', 82352563, 'rweatherill2p@dion.ne.jp', 'Mady', 'Robinia', 51935, 46033, 0, '2022-06-26', '2024-07-15');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (42263835, 'Persis', 'Serris', 'Finden', '2001-02-13', 'F', 'Daystar', 55991729, 'efinden2q@who.int', 'Valeria', 'Eirena', 48270, 53541, 1, '2021-06-11', '2024-02-28');
insert into PERSONA (CI, NOMBRE, AP_P, AP_M, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO, CORREO, PADRE, MADRE, SERIE_CI, SECCION_CI, ESTADO, EXPEDIDO, VIGENTE) values (36991952, 'Marcos', 'Bougen', 'Martinie', '1990-08-26', 'M', 'Sommers', 87028253, 'cmartinie2r@irs.gov', 'Maxy', 'Caz', 50128, 60460, 0, '2022-08-13', '2024-08-24');





insert into PERSONAL (ESTADO, ID_PER) values (1, 2);
insert into PERSONAL (ESTADO, ID_PER) values (1, 3);
insert into PERSONAL (ESTADO, ID_PER) values (1, 7);
insert into PERSONAL (ESTADO, ID_PER) values (1, 6);
insert into PERSONAL (ESTADO, ID_PER) values (1, 9);
insert into PERSONAL (ESTADO, ID_PER) values (1, 10);
insert into PERSONAL (ESTADO, ID_PER) values (1, 12);
insert into PERSONAL (ESTADO, ID_PER) values (1, 19);
insert into PERSONAL (ESTADO, ID_PER) values (1, 4);
insert into PERSONAL (ESTADO, ID_PER) values (1, 13);

INSERT INTO HORARIO(HORA_ENTRADA,HORA_SALIDA) VALUES
('2023-05-25T06:30:00', '2023-05-25T14:30:00'),
('2023-05-25T14:30:00', '2023-05-25T22:30:00'),
('2023-05-25T22:30:00', '2023-05-26T06:30:00'); 

SELECT *FROM HORARIO



INSERT INTO EQUIPO (MARCA, No_SERIE, DESCRIPCION) VALUES ('Samsung', 12345, 'Telfono mvil Galaxy S21');
INSERT INTO EQUIPO (MARCA, No_SERIE, DESCRIPCION) VALUES ('Apple', 67890, 'Porttil MacBook Pro');
INSERT INTO EQUIPO (MARCA, No_SERIE, DESCRIPCION) VALUES ('Sony', 24680, 'Televisor LED 55 pulgadas');
INSERT INTO EQUIPO (MARCA, No_SERIE, DESCRIPCION) VALUES ('LG', 13579, 'Monitor UltraWide 34 pulgadas');
INSERT INTO EQUIPO (MARCA, No_SERIE, DESCRIPCION) VALUES ('Lenovo', 98765, 'Tablet Android Yoga Tab');
INSERT INTO EQUIPO (MARCA, No_SERIE, DESCRIPCION) VALUES ('Dell', 24680, 'Porttil Inspiron 15');
INSERT INTO EQUIPO (MARCA, No_SERIE, DESCRIPCION) VALUES ('HP', 13579, 'Impresora LaserJet Pro');
INSERT INTO EQUIPO (MARCA, No_SERIE, DESCRIPCION) VALUES ('Acer', 98765, 'Monitor Gaming 27 pulgadas');
INSERT INTO EQUIPO (MARCA, No_SERIE, DESCRIPCION) VALUES ('Asus', 23456, 'Router WiFi Dual Band');
INSERT INTO EQUIPO (MARCA, No_SERIE, DESCRIPCION) VALUES ('Microsoft', 78901, 'Surface Pro 7');
INSERT INTO EQUIPO (MARCA, No_SERIE, DESCRIPCION) VALUES ('Sony', 34567, 'Consola PlayStation 5');
INSERT INTO EQUIPO (MARCA, No_SERIE, DESCRIPCION) VALUES ('Canon', 89012, 'Cmara Rflex EOS 80D');
INSERT INTO EQUIPO (MARCA, No_SERIE, DESCRIPCION) VALUES ('Samsung', 56789, 'Smart TV QLED 65 pulgadas');
INSERT INTO EQUIPO (MARCA, No_SERIE, DESCRIPCION) VALUES ('Logitech', 12345, 'Ratn Inalmbrico MX Master 3');
INSERT INTO EQUIPO (MARCA, No_SERIE, DESCRIPCION) VALUES ('LG', 67890, 'Altavoz Bluetooth PK7');

SELECT *FROM EQUIPO
SELECT * FROM PERSONAL
SELECT * FROM DESIGNAN
INSERT  INTO DESIGNAN (ID_PERSONAL,No_EQUIPO,FECHA) VALUES
(1,9,'2023-03-15'),
(2,8,'2023-03-15'),
(3,7,'2023-03-15'),
(4,6,'2023-03-15'),
(5,5,'2023-03-15'),
(6,4,'2023-03-15'),
(7,3,'2023-03-15'),
(8,2,'2023-03-15'),
(9,1,'2023-03-15');

INSERT INTO AMBIENTE (ESTADO, NOMBRE, SUPERFICIE, PISO) VALUES 
('1', 'Sala de estar', '50 m', '1er piso'),
('0', 'Dormitorio principal', '25 m', '2do piso'),
('1', 'Cocina', '15 m', '1er piso'),
('1', 'Bao principal', '8 m', '1er piso'),
('0', 'Dormitorio de invitados', '20 m', '2do piso'),
('1', 'Comedor', '30 m', '1er piso'),
('1', 'Sala de juegos', '40 m', '1er piso'),
('0', 'Estudio', '12 m', '2do piso'),
('1', 'Terraza', '35 m', '1er piso'),
('1', 'Jardn', '100 m', 'Exterior');

SELECT *FROM AMBIENTE
SELECT * FROM DESIGNAN

insert into MUNICIPIO ( NOMBRE, POBLACION) values ( 'POTOSI', '800');
insert into MUNICIPIO ( NOMBRE, POBLACION) values ( 'VILLAZON', '1008');
insert into MUNICIPIO ( NOMBRE, POBLACION) values ( 'TUPIZA', '1400');
insert into MUNICIPIO ( NOMBRE, POBLACION) values ( 'LLALLAGUA', '2000');
insert into MUNICIPIO ( NOMBRE, POBLACION) values ( 'COLQUECHACA', '1600');
insert into MUNICIPIO ( NOMBRE, POBLACION) values ( 'BETANZOS', '1500');
insert into MUNICIPIO ( NOMBRE, POBLACION) values ( 'COTAGAITA', '1900');
INSERT INTO MUNICIPIO (NOMBRE, POBLACION) values ( 'SAN PEDRO DE BUENA VISTA', '1950');
insert into MUNICIPIO ( NOMBRE, POBLACION) values ( 'UYUNI', '1700');
insert into MUNICIPIO ( NOMBRE, POBLACION) values ( 'TINGUIPAYA', '1543');
insert into MUNICIPIO ( NOMBRE, POBLACION) values ( 'POCOATA', '1000');
insert into MUNICIPIO ( NOMBRE, POBLACION) values ( 'UNCIA', '260');
insert into MUNICIPIO ( NOMBRE, POBLACION) values ( 'PUNA', '1020');
insert into MUNICIPIO ( NOMBRE, POBLACION) values ( 'RAVELO', '750');
insert into MUNICIPIO ( NOMBRE, POBLACION) values ( 'VILLA DE SACACA', '900');
insert into MUNICIPIO ( NOMBRE, POBLACION) values ( 'CHAYANTA', '654');
insert into MUNICIPIO ( NOMBRE, POBLACION) values ( 'OCUR', '160');
insert into MUNICIPIO ( NOMBRE, POBLACION) values ( 'CKOCHAS', '150');
insert into MUNICIPIO ( NOMBRE, POBLACION) values ( 'TOMAVE', '457');
insert into MUNICIPIO ( NOMBRE, POBLACION) values ( 'COLCHA K', '870');
insert into MUNICIPIO ( NOMBRE, POBLACION) values ( 'CAIZA D', '989');
insert into MUNICIPIO ( NOMBRE, POBLACION) values ( 'TACOBAMBA', '541');
insert into MUNICIPIO ( NOMBRE, POBLACION) values ( 'ATOCHACKOCHAS', '856');
insert into MUNICIPIO ( NOMBRE, POBLACION) values ( 'TORO TORO', '747');
insert into MUNICIPIO ( NOMBRE, POBLACION) values ( 'PORCO', '388');
insert into MUNICIPIO ( NOMBRE, POBLACION) values ( 'VITICHI', '511');
insert into MUNICIPIO ( NOMBRE, POBLACION) values ( 'CHAQU', '458');
insert into MUNICIPIO ( NOMBRE, POBLACION) values ( 'YOCALLA', '231');
insert into MUNICIPIO ( NOMBRE, POBLACION) values ( 'CARIPUYO', '281');
insert into MUNICIPIO ( NOMBRE, POBLACION) values ( 'CHUQUIUTA', '335');
insert into MUNICIPIO ( NOMBRE, POBLACION) values ( 'ACASIO', '66');
insert into MUNICIPIO ( NOMBRE, POBLACION) values ( 'ARAMPAMPA', '99');
insert into MUNICIPIO ( NOMBRE, POBLACION) values ( 'LLICA', '512');
insert into MUNICIPIO ( NOMBRE, POBLACION) values ( 'SAN PABLO DE LIPEZ', '164');
insert into MUNICIPIO ( NOMBRE, POBLACION) values ( 'URMIRI', '583');
insert into MUNICIPIO ( NOMBRE, POBLACION) values ( 'SAN ANTONIO DE ESMORUCO', '51');
insert into MUNICIPIO ( NOMBRE, POBLACION) values ( 'TAHUA', '89');
insert into MUNICIPIO ( NOMBRE, POBLACION) values ( 'SAN AGUSTN', '24');
insert into MUNICIPIO ( NOMBRE, POBLACION) values ( 'MOJINETE', '30');
insert into MUNICIPIO ( NOMBRE, POBLACION) values ( 'SAN PEDRO DE QUEMES', '48');

SELECT *FROM MUNICIPIO
insert into PAIS ( NOMBRE, REGION, POBLACION) values ('Armenia', null, 41277);
insert into PAIS ( NOMBRE, REGION, POBLACION) values ( 'Mauritania', null, 5678);
insert into PAIS ( NOMBRE, REGION, POBLACION) values ( 'Indonesia', null, 57546);
insert into PAIS ( NOMBRE, REGION, POBLACION) values ( 'Venezuela', null, 12742);
insert into PAIS ( NOMBRE, REGION, POBLACION) values ( 'China', null, 35947);
insert into PAIS ( NOMBRE, REGION, POBLACION) values ( 'Nigeria', null, 38259);
insert into PAIS ( NOMBRE, REGION, POBLACION) values ( 'Paraguay', null, 6273);
insert into PAIS ( NOMBRE, REGION, POBLACION) values ( 'Argentina', null, 26875);
insert into PAIS ( NOMBRE, REGION, POBLACION) values ( 'Poland', null, 33144);
insert into PAIS ( NOMBRE, REGION, POBLACION) values ( 'Chile', null, 25794);
insert into PAIS ( NOMBRE, REGION, POBLACION) values ( 'Colombia', null, 29862);
insert into PAIS ( NOMBRE, REGION, POBLACION) values ( 'Lebanon', null, 27118);
insert into PAIS ( NOMBRE, REGION, POBLACION) values ( 'Cuba', null, 96486);
insert into PAIS ( NOMBRE, REGION, POBLACION) values ( 'Bolivia', null, 49152);
insert into PAIS ( NOMBRE, REGION, POBLACION) values ( 'Brazil', null, 5908);
insert into PAIS ( NOMBRE, REGION, POBLACION) values ( 'Per', null, 86527);
insert into PAIS ( NOMBRE, REGION, POBLACION) values ( 'Sweden', null, 30029);
insert into PAIS ( NOMBRE, REGION, POBLACION) values ( 'Philippines', null, 8406);
insert into PAIS ( NOMBRE, REGION, POBLACION) values ( 'France', null, 25108);
insert into PAIS ( NOMBRE, REGION, POBLACION) values ( 'Irn', null, 19949);
SELECT *FROM PAIS

insert into DEPARTAMENTO ( NOMBRE, POBLACION) values ( 'POTOS', 5066);
insert into DEPARTAMENTO ( NOMBRE, POBLACION) values ( 'COCHABAMBA', 4871);
insert into DEPARTAMENTO ( NOMBRE, POBLACION) values ( 'TARIJA', 4816);
insert into DEPARTAMENTO ( NOMBRE, POBLACION) values ( 'SANTA CRUZ', 2686);
insert into DEPARTAMENTO ( NOMBRE, POBLACION) values ( 'CHUQUISACA', 4958);
insert into DEPARTAMENTO ( NOMBRE, POBLACION) values ( 'BENI', 3698);
insert into DEPARTAMENTO ( NOMBRE, POBLACION) values ( 'PANDO', 5373);
insert into DEPARTAMENTO ( NOMBRE, POBLACION) values ( 'ORURO', 1715);
insert into DEPARTAMENTO ( NOMBRE, POBLACION) values ( 'LA PAZ', 2523);

SELECT *FROM DEPARTAMENTO


insert into DISTRITO ( NOMBRE, DESCRIPCION) values ( 'DISTRITO 1', 'primer  distrito en potosi en la zona');
insert into DISTRITO ( NOMBRE, DESCRIPCION) values ( 'DISTRITO 2', 'Cracticus nigroagularis');
insert into DISTRITO ( NOMBRE, DESCRIPCION) values ( 'DISTRITO 3', 'Lycaon pictus');
insert into DISTRITO ( NOMBRE, DESCRIPCION) values ( 'DISTRITO 4', 'Acrantophis madagascariensis');
insert into DISTRITO ( NOMBRE, DESCRIPCION) values ( 'DISTRITO 5', 'Prionace glauca');
insert into DISTRITO ( NOMBRE, DESCRIPCION) values ( 'DISTRITO 6', 'Uraeginthus granatina');
insert into DISTRITO ( NOMBRE, DESCRIPCION) values ( 'DISTRITO 7', 'Gymnorhina tibicen');
insert into DISTRITO ( NOMBRE, DESCRIPCION) values ( 'DISTRITO 8', 'Nycticorax nycticorax');
insert into DISTRITO ( NOMBRE, DESCRIPCION) values ( 'DISTRITO 9', 'Tockus flavirostris');
insert into DISTRITO ( NOMBRE, DESCRIPCION) values ( 'DISTRITO 10', 'Perameles nasuta');
insert into DISTRITO ( NOMBRE, DESCRIPCION) values ( 'DISTRITO 11', 'Redunca redunca');
insert into DISTRITO ( NOMBRE, DESCRIPCION) values ( 'DISTRITO 12', 'Oreotragus oreotragus');

--------------------------------------------------
insert into MESA (NOMBRE) values ('Mesa 1');
insert into MESA (NOMBRE) values ('Mesa 2');
insert into MESA (NOMBRE) values ('Mesa 3');
insert into MESA (NOMBRE) values ('Mesa 4');
insert into MESA (NOMBRE) values ('Mesa 5');
insert into MESA (NOMBRE) values ('Mesa 6');
insert into MESA (NOMBRE) values ('Mesa 7');
insert into MESA (NOMBRE) values ('Mesa 8');
insert into MESA (NOMBRE) values ('Mesa 9');
insert into MESA (NOMBRE) values ('Mesa 10');
insert into MESA (NOMBRE) values ('Mesa 11');
insert into MESA (NOMBRE) values ('Mesa 12');
insert into MESA (NOMBRE) values ('Mesa 13');
insert into MESA (NOMBRE) values ('Mesa 14');
insert into MESA (NOMBRE) values ('Mesa 15');
insert into MESA (NOMBRE) values ('Mesa 16');
insert into MESA (NOMBRE) values ('Mesa 17');
insert into MESA (NOMBRE) values ('Mesa 18');
insert into MESA (NOMBRE) values ('Mesa 19');
insert into MESA (NOMBRE) values ('Mesa 20');

SELECT *FROM MESA

insert into OFICIOS (NOMBRE, SUELDO, DESCRIPCION) values ('Limpieza', 7392, 'Lorem ipsum dolor sit amet consectetur, adipisicing elit.');
insert into OFICIOS (NOMBRE, SUELDO, DESCRIPCION) values ('Mantenimiento', 8178,'Lorem ipsum dolor sit amet consectetur, adipisicing elit.');
insert into OFICIOS (NOMBRE, SUELDO, DESCRIPCION) values ('Director  General Ejecutivo', 8154,'Lorem ipsum dolor sit amet consectetur, adipisicing elit.');
insert into OFICIOS (NOMBRE, SUELDO, DESCRIPCION) values ('Contador', 6384,'Lorem ipsum dolor sit amet consectetur, adipisicing elit.');
insert into OFICIOS (NOMBRE, SUELDO, DESCRIPCION) values ('Tcnico I', 5540, 'Lorem ipsum dolor sit amet consectetur, adipisicing elit.');
insert into OFICIOS (NOMBRE, SUELDO, DESCRIPCION) values ('Tcnico II', 7651,	 'Lorem ipsum dolor sit amet consectetur, adipisicing elit.');
insert into OFICIOS (NOMBRE, SUELDO, DESCRIPCION) values ('Tcnico de ayuda informtica', 2475,  'Lorem ipsum dolor sit amet consectetur, adipisicing elit.');
insert into OFICIOS (NOMBRE, SUELDO, DESCRIPCION) values ('Gerente de Sistemas de Informacin', 9800, 'Lorem ipsum dolor sit amet consectetur');
insert into OFICIOS (NOMBRE, SUELDO, DESCRIPCION) values ('Auxiliar', 4881, 'Lorem ipsum dolor sit amet consectetur, adipis');
insert into OFICIOS (NOMBRE, SUELDO, DESCRIPCION) values ('Jefe de Unidad', 3763, 'Lorem ipsum dolor sit amet consectetur, ium illum nihil.');

SELECT *FROM OFICIOS

insert into ROL (NOMBRE_ROL, DESCRIPCION) values ('usuario', 'es el usuario registrado para las elecciones')
insert into ROL (NOMBRE_ROL, DESCRIPCION) values ('administrador', 'es el encargado de administrar las elecciones')
SELECT * FROM TIPO_DE_ELECCION
INSERT INTO TIPO_DE_ELECCION VALUES
('Elecciones para Gobiernos Locales y Regionales','se realizan cada 5 aos'),
('Elecciones para la Cmara Alta','senadores y diputados se realizan cada 5 aos.'),
('Elecciones Presidenciales','se realizan cada 5 aos para elegir las autoridades de los gobiernos regionales');

SELECT * FROM PARTIDO
INSERT INTO PARTIDO VALUES (3, 'MOVIMIENTO AL SOCIALISMO','ORIENTADA TITULO || ARTICULO 8','EL TROPICO','SOCIALISTA','M.A.S-I.P.S.P','MOVIMIENTO AL SOCILISMO (MAS)');
INSERT INTO PARTIDO VALUES (3, 'CREEMOS','ORIENTADA TITULO || ARTICULO 8','SANTA CRUZ','CONSERVADURISMO SOCIAL','CREEMOS','CREEMOS(CREEMOS)');
INSERT INTO PARTIDO VALUES (3, 'COMUNIDAD CIUDADANA','ORIENTADA TITULO || ARTICULO 8','LA PAZ','SOCIOLIBERALISMO','C.C','COMUNIDAD CIUDADANA(C.C.)');
SELECT * FROM POSTULANTE

-------NOTA



------PERTENECE_PARTIDO
------POSTULANTE
------CON PROCEDIMIENTOS
INSERT INTO USUARIO(ID_PER,NOMBRE_U,CONTRASEA) VALUES
	(1,'NADA_NO_HAY_NO_EXISTEN','IDEAS==NULL'),
	(2,'YA_NO_SE_MEOCURRE_PONER_NADA','COLOCA_MAYATAAAA'),
	(3,'JORGITO','NO_DUERMO_PPOR_LAS_NOCHES'),
	(4,'CORREDOR_CAFE','NOSE_NO_TENGO'),
	(5,'CONDORITO','DEF_ENIG$$#27'),
	(6,'IAM_RODOLFO','CALLE_CANIETE'),
	(7,'ALIAS_EL_BRAYAN','ESTEBAN_DIDO'),
	(8,'JUANCITO_PITNO','PORQUE_SI$#'),
	(9,'HIERBAS@MAURICIO','SE_ME_OLVIDO'),
	(10,'ELIOOOOOO','I_AM_BOLIVIA');
